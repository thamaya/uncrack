package com.demo.myapplication;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import me.piebridge.GenuineActivity;

import static com.demo.myapplication.UData.HOME_API;
import static com.demo.myapplication.UData.keyValue;

public class MainActivity extends GenuineActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(App.TAG, "onCreate: " + keyValue() + HOME_API());

        runOnUiThread(() ->
            Toast.makeText(MainActivity.this, keyValue(), Toast.LENGTH_SHORT).show());
    }
}
