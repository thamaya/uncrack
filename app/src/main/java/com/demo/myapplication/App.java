package com.demo.myapplication;

import android.annotation.SuppressLint;

import me.piebridge.GenuineApplication;

public class App extends GenuineApplication {

    public static final String TAG = "unCrack";

    @SuppressLint("StaticFieldLeak")
    private static App instance = null;

    public static App getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}
